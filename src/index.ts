import express from 'express'
import { createClient } from 'redis'
import bodyParser from 'body-parser'
import crypto from 'crypto'
import dotenv from 'dotenv'

dotenv.config();

const PORT = process.env.PORT || 3000;
const ACCESS_TOKEN = "access_token";
const REFRESH_TOKEN = "refresh_token";
const client = createClient();
const ACCESS_TOKEN_LIFESPAN = 60*5;
const REFRESH_TOKEN_LIFESPAN = 60*60*24;

type UserModel = {
  userId : number,
  username: string,
  password: string,
  fullName : string,
  npm : string,
}

type ClientModel = {
  clientId: string,
  clientSecret: string
}

const dummyUsersDB: UserModel[] = [
  {
    'userId': 1,
    'username': 'unicorn',
    'password': 'coba123',
    'fullName': 'Budi Anduk',
    'npm': '1406123456'
  }
];

const dummyClientsDB: ClientModel[] = [
  {
    'clientId': '7162',
    'clientSecret': '1231'
  }
];

const userIsValid = (username: string, password: string) : Boolean => 
  (dummyUsersDB.filter( user => user.username === username ).length > 0 && password === dummyUsersDB.filter( user => user.username === username )[0].password);

const clientIsValid = (clientId: string, clientSecret: string) : Boolean =>
(dummyClientsDB.filter( client => client.clientId === clientId ).length > 0 && clientSecret === dummyClientsDB.filter( client => client.clientId === clientId )[0].clientSecret);

(async () => {
  client.on('error', (err) => console.log('Redis Client Error', err))
  await client.connect()
})();

const app = express()

app.use(bodyParser.urlencoded({ extended: true  }));
app.use(bodyParser.json());

const formatKey = (key: string, field: string) => {
  return `${key}:${field}`;
};

const generateToken = (str: string) => {
  return crypto.createHash("sha1").update(`${str}:${Math.random()}`, "binary").digest("hex");
}

app.get('/oauth/resource', (req, res) => {
  let authHeader = req.headers.authorization

 if (authHeader && authHeader !== null) {
  try {
    const accessToken = authHeader.split(' ')[1];
    (async () => {
      const accessTokenData = await client.get(formatKey(ACCESS_TOKEN, accessToken));
      if (accessTokenData && accessTokenData !== null) {
        res.json(JSON.parse(accessTokenData));
      }
      else {
        res.status(401);
        res.json({"error" : "invalid_token","error_description" : "Token Salah masbro"})
      }
    })();
  } catch (e) {
    res.status(401);
    res.json({"error" : "invalid_token","error_description" : "Token Salah masbro"})
  }
 } else {
   res.status(401);
  res.json({"error" : "invalid_token","error_description" : "Token Salah masbro"})
 }
})

app.post('/oauth/token', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const clientId = req.body.client_id;
  const clientSecret = req.body.client_secret;
  
  if (!userIsValid(username, password)) {
    res.status(401);
    res.json({"error" : "invalid_request","error_description" : "Username atau password salah"})
    return
  }
  
  if (!clientIsValid(clientId, clientSecret)) {
    res.status(401);
    res.json({"error" : "invalid_request","error_description" : "Client id atau client secret salah"})
    return
  }

  const accessToken = generateToken(username);
  const refreshToken = generateToken(accessToken);
  const userData = dummyUsersDB.filter( user => user.username === username)[0];

  (async () => {
    await client.set(formatKey(ACCESS_TOKEN, accessToken), JSON.stringify({
      "access_token" : accessToken,
      "client_id" : clientId,
      "user_id" : userData.userId,
      "full_name" : userData.fullName,
      "npm" : userData.npm,
      "expires" : null,
      "refresh_token" : refreshToken
    }), {
      EX: ACCESS_TOKEN_LIFESPAN
    });
    await client.set(formatKey(REFRESH_TOKEN, refreshToken), JSON.stringify({username: username}), {
      EX: REFRESH_TOKEN_LIFESPAN
    });
    res.json({
      "access_token" : accessToken,
      "expires_in" : ACCESS_TOKEN_LIFESPAN,
      "token_type" : "Bearer",
      "scope" : null,
      "refresh_token" : refreshToken
    });
  })();
  })

app.listen(PORT, () => console.log(`listening on port ${PORT}`))